import { Component, OnInit } from '@angular/core';
import {StructuresService} from '../services/structures.service';
import { Router } from '@angular/router';
import {Structure} from '../model/structure';

@Component({
  selector: 'app-structures',
  templateUrl: './structures.component.html',
  styleUrls: ['./structures.component.css'],
  providers: [StructuresService]
})
export class StructuresComponent implements OnInit {

  constructor(private router: Router, private structuresService:StructuresService) { }

  structures:Array<Structure> = new Array<Structure>();
  structuresResponse: [];

  ngOnInit() 
  {
    this.structuresService.GetStructures().then(response => {
      this.structuresResponse =  response.structures
      this.structures = this.structuresResponse
    }).catch(response => {
      console.log(response);
    })
  }
}
