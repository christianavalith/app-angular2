import { Injectable } from '@angular/core';
import {HttpHeaders,HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class StructuresService {

  private url:string = "https://age-of-empires-2-api.herokuapp.com/api/v1/structures"

  constructor(private http:HttpClient) { }

  public GetStructures():Promise<any>
  {
    let headers = new HttpHeaders;

    headers.append("content-type","application/x-www-form-uncode; charset=utf-8");

    return this.http.get(this.url,{headers:headers}).toPromise();
  }
}
