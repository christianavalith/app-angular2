import { Injectable } from '@angular/core';
import {HttpHeaders,HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class CivilizationsService  {

  private url:string = "https://age-of-empires-2-api.herokuapp.com/api/v1/civilizations"

  constructor(private http:HttpClient) { }

  public GetCivilization():Promise<any>
  {
    let headers = new HttpHeaders;

    headers.append("content-type","application/x-www-form-uncode; charset=utf-8");

    return this.http.get(this.url,{headers:headers}).toPromise();
  }
}
